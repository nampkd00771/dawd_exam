package com.example.dawd_exam.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dawd_exam.R;
import com.example.dawd_exam.database.DBHelper;

public class AddProductActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edName;
    private EditText edQuantity;
    private Button btAdd;
    private Button btView;
    private DBHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        initViews();

        db = new DBHelper(this);
        db.getReadableDatabase();
    }

    private void initViews() {
        edName = findViewById(R.id.edName);
        edQuantity = findViewById(R.id.edQty);
        btAdd = findViewById(R.id.btAdd);
        btView = findViewById(R.id.btView);
        btAdd.setOnClickListener(this);
        btView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btAdd:
                AddProduct();
                break;
            case R.id.btView:
                ViewProduct();
                break;
            default:
                break;
        }
    }

    private void ViewProduct() {
        Intent intent = new Intent(AddProductActivity.this, LoadProductActivity.class);
        startActivity(intent);
    }

    private void AddProduct() {
        if (edName.getText().toString().isEmpty()){
            Toast.makeText(this,"Please enter product name", Toast.LENGTH_LONG).show();
            return;
        }
        if (edQuantity.getText().toString().isEmpty()){
            Toast.makeText(this,"Please enter number quantity", Toast.LENGTH_LONG).show();
            return;
        }

        String isAdd = db.addQuantity(edName.getText().toString(), Integer.parseInt(edQuantity.getText().toString()));
        Toast.makeText(this, isAdd, Toast.LENGTH_SHORT).show();
    }
}